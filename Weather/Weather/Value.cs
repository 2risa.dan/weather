﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather
{
    public class Value
    {
        public static IList<Value> Values { set; get; }
        public string datetime { private set; get; }
        public double temperature { private set; get; }
        public double humidity { private set; get; }
        public double pressure { private set; get; }
        public Value(string datetime, double temperature, double humidity, double pressure)
        {
            this.datetime = datetime;
            this.humidity = humidity;
            this.temperature = temperature;
            this.pressure = pressure;
        }
    }
}
