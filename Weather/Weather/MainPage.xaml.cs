﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Weather
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public static string apikey = "mikkey";
        public static string apiAddress = "http://192.168.0.110:8787";
        public static string count = "20";
        public MainPage()
        {
            Value.Values = new List<Value>();
           /* Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));
            Value.Values.Add(new Value("5", 5, 5, 5));*/
            InitializeComponent();
            WebClient cl = new WebClient();
            apiAddress = cl.DownloadString("http://mik.cekuj.net/api.txt");
            Refresh();
            listValues.RefreshCommand = new Command(() => {
                Refresh();
                listValues.IsRefreshing = false;
            });
        }
        /*public static string apikey = "mikkey"; { private set; get; }
        public static string apiaddress = "http://192.168.0.110:8787/api";
        public static string count = "20";*/
       // public static string api = "http://192.168.0.110:8787/api?key=mikkey&count=20";
        public async void Refresh()
        {
            using (var httpClient = new HttpClient())
            {
                try
                {
                    string json = await httpClient.GetStringAsync(apiAddress + "/weather/get?key=" + apikey+"&count=" + count);
                    Value.Values = JsonConvert.DeserializeObject<List<Value>>(json);
                    labelTmp.Text = Math.Round(Value.Values[0].temperature, 2)  + " °C";
                    labelHum.Text = Math.Round(Value.Values[0].humidity, 2) + " %";
                    labelPress.Text = Math.Round(Value.Values[0].pressure, 2) + "hPa";
                    listValues.ItemsSource = Value.Values;
                }
                catch
                {
                    Debug.WriteLine("ERROR: Unreachable server");
                }
                if(Value.Values.Count == 0)
                {
                    loadButton.IsVisible = true;
                }
                else
                {
                    loadButton.IsVisible = false;
                }
            }
        }

        private void LoadButton_Clicked(object sender, EventArgs e)
        {
            Refresh();
        }

        async private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
           // await Navigation.PushAsync(new NavigationPage(new Settings()));
            await Navigation.PushAsync(new Settings());
            
            //Title = "Settings";
        }
    }
}
