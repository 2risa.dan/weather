﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Weather
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : ContentPage
    {
        public Settings()
        {
            InitializeComponent();
            e_apiaddress.Text = MainPage.apiAddress;
            e_apikey.Text = MainPage.apikey;
            e_count.Text = MainPage.count;
        }

        private async void Save_Clicked(object sender, EventArgs e)
        {
            MainPage.apiAddress = e_apiaddress.Text ;
            MainPage.apikey = e_apikey.Text ;
            MainPage.count =  e_count.Text;
            //await Navigation.PushAsync(new MainPage());
            await Navigation.PopAsync();
        }
    }
}